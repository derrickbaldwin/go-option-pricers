package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

type AsianEquityOption struct {
	optType string // Call or Put
	avg     string // averaging method
	S0      float64 // spot price
	K       float64 // strike
	r       float64 // rate
	sigma   float64 // volatility
	tma     float64 // time to maturity (years)
}

func (a *AsianEquityOption) Price() float64 {

	rand.Seed(time.Now().UTC().UnixNano())
	const T = 100.0
	const N = 10000
	dt := a.tma / T

	/* Initialization */

	// configure asset price paths (N simulations / T time steps)
	// and anchor starting spot price
	S := [N][T]float64{}
	for n := 0; n < N; n++ {
		S[n][0] = a.S0
	}

	// initialize averaging vector
	A := [N]float64{}

	/* Simulation */

	// simulate price paths under Euler and Milstein schema
	// and average each price path
	for n := 0; n < N; n++ {
		for t := 1; t < T; t++ {
			dw := rand.NormFloat64() * math.Sqrt(dt)
			z0 := (a.r - 0.5*a.sigma*a.sigma) * S[n][t-1] * dt
			z1 := a.sigma * S[n][t-1] * dw
			z2 := 0.5 * a.sigma * a.sigma * S[n][t-1] * dw * dw
			S[n][t] = S[n][t-1] + z0 + z1 + z2
		}

		switch a.avg {
		case "arithmetic":
			A[n] = arithmeticAvg(S[n][:])
		case "geometric":
			A[n] = geometricAvg(S[n][:]) // geom avg func required
		}
	}

	/* Computing the payoffs */

	P := [N]float64{}
	switch a.optType {
	case "Call":
		for n := range P {
			P[n] = math.Max(A[n]-a.K, 0)
		}
	case "Put":
		for n := range P {
			P[n] = math.Max(a.K-A[n], 0)
		}
	}

	sum := 0.0
	for n := range P {
		sum += P[n]
	}
	return math.Exp(-a.r*a.tma) * (sum / N)
}

func arithmeticAvg(p []float64) float64 {
	sum := 0.0
	for n := range p {
		sum += p[n]
	}
	return sum / float64(len(p))
}

func geometricAvg(p []float64) float64 {
	return arithmeticAvg(p)

}

func main() {
	asianOption1 := AsianEquityOption{"Call", "arithmetic", 42.0, 40.0, 0.10,
		0.20, 0.50}

	start := time.Now()
	fmt.Println(asianOption1.Price())
	fmt.Println(time.Since(start))

}
