package main

import (
	"fmt"
	"github.com/chobie/go-gaussian"
	"math"
)

type EuropeanEquityOption struct {
	optType string  // Call or Put
	S0      float64 // spot price
	K       float64 // strike
	r       float64 // rate
	sigma   float64 // volatility
	T       float64 // time to maturity (years)
}

func (s *EuropeanEquityOption) Price() float64 {

	d := gaussian.NewGaussian(0, 1)
	d1, d2 := s.d1d2()

	if s.optType == "Call" {
		return s.S0*d.Cdf(d1) - s.K*math.Exp(-s.r*s.T)*d.Cdf(d2)
	} else {
		return s.K*math.Exp(-s.r*s.T)*d.Cdf(-d2) - s.S0*d.Cdf(-d1)
	}

}

func (s *EuropeanEquityOption) Delta() float64 {

	d := gaussian.NewGaussian(0, 1)

	if s.optType == "Call" {
		return d.Cdf(s.d1())
	} else {
		return d.Cdf(s.d1()) - 1.0
	}
}

func (s *EuropeanEquityOption) Theta() float64 {

	d := gaussian.NewGaussian(0, 1)
	d1, d2 := s.d1d2()

	if s.optType == "Call" {
		return -(s.S0*N(d1)*s.sigma)/(2*math.Sqrt(s.T)) - s.r*s.K*math.Exp(-s.r*s.T)*d.Cdf(d2)
	} else {
		return -(s.S0*N(d1)*s.sigma)/(2*math.Sqrt(s.T)) - s.r*s.K*math.Exp(-s.r*s.T)*d.Cdf(-d2)
	}
}

func (s *EuropeanEquityOption) Gamma() float64 {

	d1 := s.d1()
	return s.S0 * N(d1) / (s.S0 * s.sigma * math.Sqrt(s.T))
}

func (s *EuropeanEquityOption) Vega() float64 {

	d1 := s.d1()
	return s.S0 * math.Sqrt(s.T) * N(d1)

}

func (s *EuropeanEquityOption) Rho() float64 {

	d := gaussian.NewGaussian(0, 1)
	d2 := s.d2()

	if s.optType == "Call" {
		return s.K * s.T * math.Exp(-s.r*s.T) * d.Cdf(d2)
	} else {
		return -s.K * s.T * math.Exp(-s.r*s.T) * d.Cdf(-d2)
	}
}

func (o *EuropeanEquityOption) Greeks() []float64 {
	price := o.Price()
	delta := o.Delta()
	gamma := o.Gamma()
	vega := o.Vega()
	theta := o.Theta()
	rho := o.Rho()
	return []float64{price, delta, gamma, vega, theta, rho}

}

func (o *EuropeanEquityOption) d1() float64 {
	return (math.Log(o.S0/o.K) + (o.r+o.sigma*o.sigma*0.5)*o.T) / (o.sigma * math.Sqrt(o.T))
}

func (o *EuropeanEquityOption) d2() float64 {
	return o.d1() - o.sigma*math.Sqrt(o.T)
}

func (o *EuropeanEquityOption) d1d2() (float64, float64) {
	return o.d1(), o.d2()
}

// helper
func N(x float64) float64 {
	return math.Exp((-x*x)/2) * 1 / (math.Sqrt(2 * math.Pi))
}

func main() {

	opt1 := EuropeanEquityOption{"Call", 42.0, 40.0, 0.10, 0.20, 0.50}
	opt2 := EuropeanEquityOption{"Put", 42.0, 40.0, 0.10, 0.20, 0.50}

	portfolio := []EuropeanEquityOption{opt1, opt2}

	for _, opt := range portfolio {
		fmt.Println(opt.Greeks())
	}
}
