package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

func montecarlo_blackscholes(S float64, K float64, r float64, sigma float64, T float64) float64 {

	const N = 10000 // number of simulations (price paths)
	rand.Seed(time.Now().UTC().UnixNano())

	P := [N]float64{} // terminal price vector
	C := [N]float64{} // payoff vector

	for n := range P {
		Z := rand.NormFloat64()
		z0 := (r - 0.5*sigma*sigma) * T
		z1 := sigma * math.Sqrt(T) * Z
		P[n] = S * math.Exp(z0+z1)
	}

	d := math.Exp(-r * T)
	for n := range P {
		C[n] = math.Max(P[n]-K, 0) * d
	}

	sum := 0.0
	for n := range C {
		sum += C[n]
	}

	return sum / float64(N)
}

func main() {

	price := montecarlo_blackscholes(42.0, 40.0, 0.10, 0.20, 0.50)
	fmt.Printf("%.2f\n", price)
	start := time.Now()
	fmt.Println(montecarlo_blackscholes(42.0, 40.0, 0.10, 0.20, 0.50))
	fmt.Println(time.Since(start))
}
